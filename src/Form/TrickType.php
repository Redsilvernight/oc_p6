<?php

namespace App\Form;

use App\Entity\Tag;
use App\Entity\Trick;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrickType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nom du trick',
                'attr' => [
                    'placeholder' => 'Entrez le nom du trick'
                ]
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Description du trick',
                'attr' => [
                    'placeholder' => 'Decrivez le trick',
                    'rows' => 10
                ]
            ])
            ->add('tag', EntityType::class, [
                'placeholder' => '--Choisir un tag--',
                'class' => Tag::class,
                'choice_label' => 'title'
            ])
            ->add('medias', FileType::class, [
                'label' => 'medias',
                'multiple' => true,
                'required' => false,
                'mapped' => false
            ])
            ->add('mainPicture', FileType::class, [
                'label' => "Image principale",
                'multiple' => false,
                'mapped' => false,
                'required' => false
            ])
            ->add('mainExist', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('replacePicture', FileType::class, [
                'label' => "Image de remplacement",
                'multiple' => false,
                'mapped' => false,
                'required' => false
            ])
            ->add('replaceExist', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('addVideo', CollectionType::class, [
                'entry_type' => UrlType::class,
                'entry_options' => [
                    'attr' => ['class' => 'form-group'],
                    'label' => false
                ],
                'label' => "URL d'une video",
                'mapped' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'delete_empty' => true,
                'required' => false
            ])
            ->add('singleVideo',TextType::class, [
                'mapped' => false,
                'required' => false,
                'label' => "URL d'une video",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Trick::class,
        ]);
    }
}
