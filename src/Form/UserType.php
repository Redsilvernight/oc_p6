<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add( 'urlProfilPicture', FileType::class, [
            "label" => "Photo de profil",
            "multiple" => false,
            "required" => false,
            "mapped" => false
        ])
            ->add('password', PasswordType::class, [
                "label" => "Mot de passe actuel",
                "required" => false
            ])
            ->add('newPassword', PasswordType::class, [
                "mapped" => false,
                "label" => "Nouveau mot de passe",
                "required" => false
            ])
            ->add('confirmPassword', PasswordType::class, [
                "mapped" => false,
                "label" => "Confirmez mot de passe",
                "required" => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
