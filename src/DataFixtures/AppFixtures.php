<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Medias;
use App\Entity\Tag;
use App\Entity\Trick;
use App\Entity\User;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private $tagRepository;
    private $userRepository;
    private $manager;
    private $sluger;
    private $parameter;
    private $hasheur;

    public function __construct(TagRepository $tagRepository,EntityManagerInterface $manager, SluggerInterface $sluger, UserRepository $userRepository, ParameterBagInterface $parameterBagInterface, UserPasswordHasherInterface $hasheur) {
        $this->tagRepository = $tagRepository;
        $this->manager = $manager;
        $this->sluger = $sluger;
        $this->userRepository = $userRepository;
        $this->parameter = $parameterBagInterface;
        $this->hasheur = $hasheur;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new \Bezhanov\Faker\Provider\Commerce($faker));

        $this->tagLoad();
        $this->userLoad($faker);
        $this->adminLoad($faker);

        $users = $this->userRepository->findAll();
        $tags = $this->tagRepository->findAll();

        foreach($tags as $tag) {
            for($i = 0; $i < 50; $i++) {
                $trick = new Trick;
                $title = $faker->productName();
                $trick->setTitle($title)
                    ->setContent($faker->realText(200,2))
                    ->setCreateDate(new \DateTime("NOW"))
                    ->setEditDate(new \DateTime("NOW"))
                    ->setSlug(strtolower($this->sluger->slug($title)))
                    ->setTag($tag)
                    ->setAuthor($users[array_rand($users, 1)])
                ;

                for($m = 1; $m < rand(1,11); $m++) {
                    $media = new Medias();
                    $url = "https://picsum.photos/300/200";
                    $name = md5(uniqid()).".jpg";
                    $file = $this->parameter->get('images_directory')."/".$name;
                    file_put_contents($file,file_get_contents($url));
                    $media->setName($name)
                        ->setType("image");
                    $trick->addMedia($media);
                    if ($m == 1) {
                        $trick->setMainPicture($media);
                    }

                    for($c = 1; $c < rand(8,20); $c++) {
                        $comment = new Comment();
                        $comment->setAuthor($users[array_rand($users, 1)])
                                ->setContent($faker->realText(100,2))
                                ->setCreatedAt(new \DateTime("NOW"));
                        $trick->addComment($comment);
                        $manager->persist($comment);
                    }
                    $manager->persist($media);
                    $manager->persist($trick);
                }
            }
        }
        $manager->flush();
    }

    private function tagLoad(): void
    {
        $tricks = [
            [
                "grabs", 
                "Un grab consiste à attraper la planche avec la main pendant le saut. Le verbe anglais to grab signifie « attraper. » Il existe plusieurs types de grabs selon la position de la saisie et la main choisie pour l'effectuer, avec des difficultés variables.",
                "grabs",
            ],
            [
                "rotations",
                "On désigne par le mot « rotation » uniquement des rotations horizontales ; les rotations verticales sont des flips. Le principe est d'effectuer une rotation horizontale pendant le saut, puis d'attérir en position switch ou normal.",
                "rotations",
            ],
            [
                "flips",
                "Un flip est une rotation verticale. On distingue les front flips, rotations en avant, et les back flips, rotations en arrière.",
                "flips",
            ],
            [
                "slides",
                "Un slide consiste à glisser sur une barre de slide. Le slide se fait soit avec la planche dans l'axe de la barre, soit perpendiculaire, soit plus ou moins désaxé.",
                "slides",
            ]
        ];

        foreach ($tricks as $trick) {
            $tag = new Tag();
            $tag -> setTitle($trick[0])
                ->setDescription($trick[1])
                ->setSlug($trick[2]);
            
            $this->manager->persist($tag);
        }

        $this->manager->flush();
    }

    public function userLoad($faker)
    {
        for ($u = 0; $u < 50; $u++) {
            $media = new Medias();
            $url = "https://picsum.photos/200/200";
            $name = md5(uniqid()) . ".jpg";
            $file = $this->parameter->get('profil_directory') . "/" . $name;
            file_put_contents($file, file_get_contents($url));
            $media->setName($name)
                ->setType("image");
            $user = new User();
            $user -> setUsername($faker->userName())
                ->setEmail($faker->email())
                ->setPassword($this->hasheur->hashPassword($user, $faker->password()))
                ->setProfilPicture($media);
            
            $this->manager->persist($media);
            $this->manager->persist($user);
        }
        $this->manager->flush();
    }

    public function adminLoad($faker) {
        $media = new Medias();
        $media->setName("default.png")
            ->setType("image");
        $user = new User();
        $user -> setUsername("adminTest")
            ->setEmail("florent-1@orange.fr")
            ->setPassword($this->hasheur->hashPassword($user, "testtest"))
            ->setProfilPicture($media)
            ->setIsVerified(true);
            
        $this->manager->persist($media);
        $this->manager->persist($user);
        $this->manager->flush();
    }
}
