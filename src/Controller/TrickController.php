<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Form\CommentType;
use App\Form\TrickType;
use App\Repository\UserRepository;
use App\Repository\TrickRepository;
use App\Repository\MediasRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TrickController extends AbstractController
{
    /**
     * @Route("/media/delete/{id_trick}/{id_media}", name="trick_mediaDelete")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function deleteMediaTrick($id_trick, $id_media, EntityManagerInterface $manager, TrickRepository $trickRepository, MediasRepository $mediasRepository)
    {
        $trick = Trick::deleteMedia($id_trick, $id_media,$mediasRepository, $trickRepository, $manager, $this->getParameter('images_directory'));
        return $this->redirectToRoute('trick_edit', ['slug' => $trick->getSlug()]);
    }

    /**
     * @Route("/trick/delete/{id}", name="trick_delete")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function deleteTrick($id, EntityManagerInterface $manager, TrickRepository $trickRepository)
    {
        $trick = $trickRepository->findOneById($id);
        $manager->remove($trick);
        $manager->flush();
        $this->addFlash('success', 'Figure supprimée avec succés !');
        return $this->redirectToRoute('homepage', ['_fragment' => 'div_tricks']);
    }

    /**
     * @Route("/trick/add", name="trick_add")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function add(Request $request, EntityManagerInterface $manager, SluggerInterface $slugger, ValidatorInterface $validator, UserRepository $userRepository) {
        $trick = new Trick();
        $user = $userRepository->findOneBy(['username' => $request->getSession()->get('username', [])]);

        $formTrick = $this->createForm(TrickType::class,$trick, ['validation_groups'=>"new_trick"]);

        $formTrick->handleRequest($request);
        $errors = $validator->validate($trick, null, ['new_trick']);
        if($formTrick->isSubmitted() && count($errors) == 0) {
            if($formTrick->get('medias')->getData() != null) {
                $trick->addTrick($formTrick, $slugger, $this->getParameter('images_directory'), $manager, $user);
                $this->addFlash('success', 'Figure postée avec succés !');
                return $this->redirectToRoute('homepage', ['_fragment' => 'div_tricks']);
            }
            return $this->render('trick/add.html.twig', [
            'formTrick' => $formTrick->createView(),
            'error' => "N'oubliez pas d'illustrer votre article !"
            ]);
        }
        return $this->render('trick/add.html.twig', [
            'formTrick' => $formTrick->createView()
        ]);
    }

    /**
     * @Route("/trick/edit/{slug}", name="trick_edit")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function edit($slug, TrickRepository $trickRepository, MediasRepository $mediasRepository, Request $request, EntityManagerInterface $manager, ValidatorInterface $validator) {
        $trick = $trickRepository->findOneBy(['slug' => $slug]);
        $originTitle = $trick->getTitle();
        $originContent = $trick->getContent();

        $editTrick = $this->createForm(TrickType::class,$trick, ['validation_groups' => "new_trick"]);

        $editTrick->handleRequest($request);
        $errors = $validator->validate($trick, null, ['new_trick']);
        if ($editTrick->isSubmitted() && count($errors) == 0) {
            $trick->editTrick($editTrick,$this->getParameter('images_directory'), $mediasRepository, $manager);
            $this->addFlash('success', 'Figure éditée avec succés !');
            return $this->redirectToRoute('trick_show', ['slug' => $trick->getSlug()]);
        }

        $trick->setTitle($originTitle);
        $trick->setContent($originContent);
        return $this->render('trick/edit.html.twig', [
            'trick' => $trick,
            'editTrick' => $editTrick->createView()
        ]);
    }
    
    /**
     * @Route("/trick/{slug}", name="trick_show")
     */
    public function show(TrickRepository $trickRepository, $slug, Request $request, UserRepository $userRepository, EntityManagerInterface $manager) {
        $trick = $trickRepository->findOneBy(['slug' => $slug]);
        $comment = new Comment();
        $commentForm = $this->createForm(CommentType::class,$comment);

        $commentForm->handleRequest($request);

        if($commentForm->isSubmitted()) {
            $trick->postComment($userRepository->findOneBy(['username' => $request->getSession()->get('username', [])]),$commentForm, $manager);
            $this->addFlash('success', 'Commentaire posté avec succés !');
        }
        return $this->render('trick/show.html.twig',[
            'trick' => $trick,
            'commentForm' => $commentForm->createView()
        ]);
    }


}
