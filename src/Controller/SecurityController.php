<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('target_path');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/user", name="security_edit")
     * @IsGranted("ROLE_ADMIN", statusCode = 403, message="Vous devez être connecté pour accéder à cette page !");
     */
    public function editUser(UserRepository $userRepository, Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $hasheur)
    {
        $user = new User();
        $updatedUser = $userRepository->findOneBy(['username' => $request->getSession()->get('username')]);

        $editUser = $this->createForm(UserType::class, $user);

        $editUser->handleRequest($request);
        if($editUser->isSubmitted())
        {
            if($editUser->get('urlProfilPicture')->getData() != "")
            {
                $updatedUser->updatePicture($editUser, $this->getParameter('profil_directory'),$manager,$updatedUser);
                $this->addFlash('success', 'Votre photo de profil à était mis à jour');
            }
            if($editUser->get('password')->getData() != "")
            {
                if ($hasheur->isPasswordValid($updatedUser,$editUser->get('password')->getData()) && $editUser->get('newPassword')->getData() == $editUser->get('confirmPassword')->getData())
                {
                    $updatedUser->setPassword($hasheur->hashPassword($updatedUser, $editUser->get("newPassword")->getData()));
                    $manager->flush();
                    $this->addFlash('success', 'Votre mot de passe à était mis à jour');
                    return $this->redirectToRoute("app_logout");
                }
            }
        }
        
        return $this->render('security/edit_user.html.twig',[
            "editUserForm" => $editUser->createView(),
        ]);
    }
}
