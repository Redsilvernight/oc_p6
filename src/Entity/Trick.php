<?php

namespace App\Entity;

use App\Repository\MediasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Repository\TrickRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @ORM\Entity(repositoryClass=TrickRepository::class)
 * @UniqueEntity(fields= {"title"}, message= "OH NON ! On vous a devance, cette figure est deja presentee.", groups= {"new_trick"})
 */
class Trick
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Vous oubliez de nommer votre figure !", groups={"new_trick"})
     * @Assert\Length(min=3,max=255, minMessage="Une figure doit avoir au moins 3 caractères.", maxMessage="Une figure doit avoir moins de 255 caractères.", groups={"new_trick"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Vous oubliez de décrire votre figure !", groups={"new_trick"})
     * @Assert\Length(min=3,max=20000, minMessage="Une figure doit avoir au moins 3 lettres.", maxMessage="Une figure doit avoir moins de 20000 caractères.", groups={"new_trick"})
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime")
     */
    private $edit_date;

    /**
     * @ORM\ManyToOne(targetEntity=Tag::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tag;

    /**
     * @ORM\OneToMany(targetEntity=Medias::class, mappedBy="tricks", orphanRemoval=true, cascade={"remove"})
     */
    private $medias;

    /**
     * @ORM\OneToOne(targetEntity=Medias::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $mainPicture;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, cascade={"persist"}, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="trick", orphanRemoval=true)
     */
    private $comments;

    public function __construct()
    {
        $this->medias = new ArrayCollection();
        $this->author = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreate_date(): ?\DateTimeInterface
    {
        return $this->create_date;
    }

    public function setCreateDate(\DateTimeInterface $create_date): self
    {
        $this->create_date = $create_date;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getEdit_date(): ?\DateTimeInterface
    {
        return $this->edit_date;
    }

    public function setEditDate(\DateTimeInterface $edit_date): self
    {
        $this->edit_date = $edit_date;

        return $this;
    }

    public function getTag(): ?Tag
    {
        return $this->tag;
    }

    public function setTag(?Tag $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * @return Collection|Medias[]
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    public function addMedia(Medias $media): self
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
            $media->setTricks($this);
        }

        return $this;
    }

    public function removeMedia(Medias $media): self
    {
        if ($this->medias->removeElement($media)) {
            // set the owning side to null (unless already changed)
            if ($media->getTricks() === $this) {
                $media->setTricks(null);
            }
        }

        return $this;
    }

    public function getMainPicture(): ?Medias
    {
        return $this->mainPicture;
    }

    public function setMainPicture(Medias $mainPicture): self
    {
        $this->mainPicture = $mainPicture;

        return $this;
    }

    public function editTrick($form, $path, MediasRepository $mediasRepository, EntityManagerInterface $manager): self
    {
        $this->setEditDate(new \DateTime("NOW"));
        $this->replaceMedia($form,$path,$manager, $mediasRepository);
        $this->newPicture($form, $path);
        $this->selectMainPicture($form, $mediasRepository);

        $manager->flush();

        return $this;
    }

    private function replaceMedia($form,$path,EntityManagerInterface $manager, MediasRepository $mediasRepository): void
    {
        $replacePicture = $form->get('replacePicture')->getData();
        $replaceVideo = $form->get('singleVideo')->getData();

        if ($replacePicture != "") {
            $file = md5(uniqid()) . '.' . $replacePicture->guessExtension();
            $mediaToReplace = $mediasRepository->findOneById($form->get('replaceExist')->getData());

            $replacePicture->move($path, $file);

            $media = new Medias();
            $media->setName($file)
                ->setType("image");
            $this->addMedia($media);
            $this->removeMedia($mediaToReplace);

            $manager->persist($media);
        }
        if ($replaceVideo != "") {
            $mediaToReplace = $mediasRepository->findOneById($form->get('replaceExist')->getData());
            $media = new Medias();
            $media->setName($replaceVideo)
                ->setType("video");
            $this->addMedia($media);
            $this->removeMedia($mediaToReplace);

            $manager->persist($media);
        }

    }

    private function newPicture($form, $path): void
    {
        $mainPicture = $form->get('mainPicture')->getData();
        if ($mainPicture != "") {
            $file = md5(uniqid()) . '.' . $mainPicture->guessExtension();

            $mainPicture->move($path, $file);

            $media = new Medias();
            $media->setName($file)
                ->setType("image");
            $this->addMedia($media);
            $this->setMainPicture($media);
        }
    }

    private function selectMainPicture($form, MediasRepository $mediasRepository): void
    {
        $mainExist = $form["mainExist"]->getData();

        if ($mainExist != "") {
            $media = $mediasRepository->findOneBy(["id" => $mainExist]);
            $this->setMainPicture($media);
        }
    }

    public function addTrick($form, SluggerInterface $slugger ,$path, EntityManagerInterface $manager, $author): self
    {
        $this->setCreateDate(new \Datetime("NOW"))
            ->setSlug(strtolower($slugger->slug($form->get('title')->getData())))
            ->setEditDate(new \Datetime("NOW"))
            ->newMedias($form->get('medias')->getData(), $path, $manager)
            ->newVideos($form->get('addVideo')->getData(), $manager)
            ->setMainPicture($this->getMedias()[0])
            ->setAuthor($author);

        $manager->persist($this);
        $manager->flush();

        return $this;
    }

    private function newVideos($videos,$manager): self
    {
        if($videos != null) {
            foreach($videos as $video) {
                $media = new Medias();
                $media->setName($video)
                    ->setType("video");

                $manager->persist($media);
                $this->addMedia($media);
            }
        }
        return $this;
    }
    private function newMedias(array $medias, $path, $manager): self
    {
        foreach ($medias as $newMedias) {
            $file = md5(uniqid()) . '.' . $newMedias->guessExtension();

            $newMedias->move($path, $file);

            $media = new Medias();
            $media->setName($file)
                ->setType("image");
            $this->addMedia($media);
            $manager->persist($media);
        }

        return $this;
    }

    public static function deleteMedia($id_trick, $id_media, $mediasRepository, $trickRepository, $manager, $path): self
    {
        $media = $mediasRepository->findOneById($id_media);
        $trick = $trickRepository->findOneById($id_trick);
        if ($media->getId() == $trick->getMainPicture()->getId())
        {
            $trick->setMainPicture($trick->getMedias()[0]);
        }
        unlink($path."/".$media->getName());
        $trick->removeMedia($media);
        $manager->flush();

        return $trick;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setTrick($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->removeElement($comment)) {
            // set the owning side to null (unless already changed)
            if ($comment->getTrick() === $this) {
                $comment->setTrick(null);
            }
        }

        return $this;
    }

    public function postComment(User $author, $commentForm, $manager): self
    {
        $content = $commentForm->get('content')->getData();
        $comment = new Comment();
        $comment->setAuthor($author)
                ->setContent($content)
                ->setCreatedAt(new \DateTime("NOW"));

        $this->addComment($comment);

        $manager->persist($comment);
        $manager->flush();

        return $this;
    }

    public function countImages(): int
    {
        $counter = 0;
        foreach ($this->getMedias() as $media) {
            if ($media->getType() == "image") {
                $counter++;
            }
        }
        return $counter;
    }
}
