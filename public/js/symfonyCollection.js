// add-collection-widget.js
jQuery(document).ready(function () {
    jQuery('.add-field').click(function (e) {
        var list = jQuery(jQuery(this).attr('data-list-selector'));
        var counter = list.data('widget-counter') || list.children().length;
        var newWidget = list.attr('data-prototype');
        newWidget = newWidget.replace(/__name__/g, counter);
        counter++; 
        list.data('widget-counter', counter);
        var newElem = jQuery(list.attr('data-widget-tags')).html(newWidget).append('<button type="button" onClick="removeField(this.id)" id="btn_' + (counter-1) +'"class="h-75 btn danger-hover col-1 p-0"><i class="far fa-trash-alt fa-2x"></i></button>');
        newElem.appendTo(list);
        $('#li_trick_addVideo_').attr('id','li_trick_addVideo_' + (counter-1));
    });
});

function removeField(id) {

    $('#li_trick_addVideo_' + id.split("_")[1]).remove();
}