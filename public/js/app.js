
    $( document ).ready(function() {
        if($('.content').attr('id') == "content-comment")
        {
            var items = 10;
        }
        else if ($('.content').attr('id') == "content-trick")
        {
            var items = 15;
        }
        $('.content').simpleLoadMore({
            item: '.items',
            count: items,
            itemsToLoad: items,
            btnHTML: '<button type="button" id="btn-showMore" class="col-xl-4 mb-5 mt-2 btn btn-primary pe-2 ps-2"><i class="fas fa-spinner me-2 mb-2"></i>Afficher plus ( {showing} / {total} )</button>',
            easing: "slide",
            btnWrapper: false
        });
    });
    
    function getId(url) {
            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length === 11) {
                return match[2];
            } else {
                return 'error';
            }
        }
    function convertUrl(url, id, height) {
        var myId = getId(url);

        $("#videoSrc").html(myId);

        $('#media_' + id).html('<iframe src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen width="320" height="'+height+'"></iframe>');
    }
    function previewUrl(url, id) {
        var myId = getId(url);
        $('#previewContent').append('<iframe src="//www.youtube.com/embed/' + myId + '" frameborder="0" allowfullscreen width="200" height="100"></iframe>');
    }
    function showMedia() {
        $('#mobile-medias').hide();
        $('.desktop-images').show();
        $('#desktop-medias').css('visibility', 'visible');
        $('#mobile-close-medias').css('visibility', 'visible');
        }

    function closeMedia() {
        $('#mobile-medias').show();
        $('.desktop-images').css('display','');
        $('#desktop-medias').css('visibility', '').css('visibility', 'colapse');
        $('#mobile-close-medias').css('visibility', '').css('visibility', 'colapse')
            }

    function displayMedia(media) {
        $('#display-media').css('background-image', "url(" + $(media).attr("src") + ")");
        $('*').toggleClass("display-active", false);
        $('#' + $(media).attr("id")).toggleClass("display-active");
        }


function uploadMainPicture() {
    $('#actionContent').hide();
    $('#selectMainPicture').hide();
    $('#selectFile').show();
    $('#importMainPicture').show();
    $('#submitMainEdit').show();
}
function selectExistingPicture() {
    $('#actionContent').hide();
    $('#importMainPicture').hide();
    $('#selectFile').show();
    $('#selectMainPicture').show();
    $('#submitMainEdit').show();
}
function previewPicture(input) {
    if (input.files && input.files[0]) {
        $('.selectFile').append('</br><img src="" alt="Image Principale" id="mainPreview"/>');
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#mainPreview').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function previewReplace(input) {
    if (input.files && input.files[0]) {
        $("#trick_replacePicture").hide();
        $('#selectReplace').append('</br><img class="d-flex" src="" alt="Image de remplacement" id="replacePreview">');
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#replacePreview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function resetEditModal() {
    $('#selectFile').hide();
    $('#submitMainEdit').hide();
    $('#selectMainPicture').hide();
    $('#importMainPicture').hide();
    $('#actionContent').show();
}
function imgSelect(img) {
    $('*').toggleClass("select-active", false);
    $('#' + img).toggleClass("select-active");
    $('#trick_mainExist').val(img.split('_')[1]);
}

function resetDeleteSelect() {
    $("*").toggleClass("delete-active", false);
}
