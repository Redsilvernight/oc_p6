# Snowtricks

## Generals Infos
Projet n°6 de la formation open classroom PHP/Symfony. Un blog simple de présentation de figure de snowboard.

## Technologies
***
This project was made with :
* [HTML]: Version 5
* [CSS]: Version 3
* [JAVASCRIPT]: Version ES12
* [PHP]: Version 8.0.10
* [Symfony]: Version 6.1

* [BOOTSTRAP]: Version 5.1
* [FONT-AWESOME]: Version 5.15
* [JQUERY]: Version 3.6

## installation
***
Install this project with git and composer
***
* Clone repository
```
cd existing_repo
git remote add origin https://gitlab.com/Redsilvernight/oc_p6.git
git branch -M main
git push -uf origin main
```
* Update composer
```
$ composer update
```
* create database
```
$ symfony console doctrine:database:create
```
* Install database
```
$ symfony console doctrine:migration:migrate
```
* Load Fixtures
```
$ symfony console doctrine:fixtures:loads
```
* Start symfony server
```
$ symfony server:start -d
